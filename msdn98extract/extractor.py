import logging
from os import PathLike
from pathlib import Path
import time
from typing import List

from bs4 import BeautifulSoup, Tag
from chm.chm import CHMFile

from .topic import Topic

LOGGER = logging.getLogger(__name__)

class Extractor:
    '''MSDN 98 KnowledgeBase archive extractor.'''
    def __init__(self, doc_path: PathLike, out_path: PathLike,
                 input_type: str):
        self.doc = CHMFile()
        if not self.doc.LoadCHM(doc_path):
            raise IOError(f'could not load CHM file: {doc_path}')

        self.out = Path(out_path)
        self.out.mkdir(exist_ok=True)
        self.input_type = input_type

    def extract_topics(self) -> List[Topic]:
        '''Extract articles from the help file.'''
        encoding = self.doc.GetEncoding() or 'cp1252'
        html = self.doc.GetTopicsTree()
        # The topics tree is quite large (>11 MiB of HTML).
        # lxml has a noticable speed boost compared to html.parser.
        LOGGER.info('Parsing topics tree (this may take a while)...')
        start_time = time.time()
        soup = BeautifulSoup(html, 'lxml')
        parse_time = time.time()
        LOGGER.info('Tree parsed in %f seconds', parse_time - start_time)

        # Hardcode this to skip over the lonely "about" topic for 1998 version
        offset = 3 if self.input_type == '1998' else 1
        root: Tag = soup.html.body.contents[offset]

        children: List[Topic] = []

        for parent_tag in root.children:
            # Ignore whitespace
            if not isinstance(parent_tag, Tag):
                continue

            # Ignore "About" topics
            if len(parent_tag.contents) < 4:
                continue

            parent_tag: Tag

            # 0: whitespace
            # 1: parent information
            # 2: whitespace
            # 3: child tags
            # 4: whitespace
            # For most of 1999, the initial whitespace is not present
            if len(parent_tag.contents) < 4:
                start = parent_tag.contents[2]
            else:
                start = parent_tag.contents[3]
                if not isinstance(start, Tag):
                    start = parent_tag.contents[2]
            for child_tag in start.find_all('li'):
                if not isinstance(child_tag, Tag):
                    continue

                child_tag: Tag

                obj: Tag = child_tag.object
                name = obj.find('param', {'name': 'Name'}).get('value')
                found = obj.find('param', {'name': 'Local'})
                if not found:
                    LOGGER.info('skipping child with no Local: %s', obj)
                    continue
                local = found.get('value')

                info = self.doc.ResolveObject(f'/{local}'.encode())[1]

                if info is None:
                    raise IOError(f'cannot resolve object: /{local}')

                child_html_encoded = self.doc.RetrieveObject(info)[1]
                try:
                    child_html = child_html_encoded.decode(encoding)
                except UnicodeDecodeError:
                    # At least one article (Q234748) needs this
                    LOGGER.info('overriding to iso-8859-1 for: %s', local)
                    child_html = child_html_encoded.decode('iso-8859-1')

                children.append(Topic(name, local, child_html))

        end_time = time.time()
        LOGGER.info('Topics parsed in %f seconds', end_time - parse_time)
        return children
