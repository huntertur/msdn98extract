from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import date
from pathlib import Path
from typing import List

from bs4 import BeautifulSoup, Tag

from .topic import Topic

@dataclass
class Output:
    '''Output path and content for an article.'''
    path: Path
    content: str

class Converter(ABC):
    '''Converter from the base output format to another.'''
    @abstractmethod
    def convert_topic(self, topic: Topic) -> Output:
        '''Convert an article.'''
        raise NotImplementedError

class NullConverter(Converter):
    '''Converter that outputs the basic structure as-is.'''
    def convert_topic(self, topic: Topic) -> Output:
        '''Output the article as-is.'''
        # Remove the leading 'Source/' directory
        path = Path(*Path(topic.local).parts[1:])
        return Output(path, topic.html)

class KbarchiveConverter(Converter):
    '''Converter that outputs in the kbarchive txt format.'''
    template = '''\
DOCUMENT:{kbid:7} {when}  [{dirname}]
TITLE   :{title}
PRODUCT :{product}
PROD/VER:
OPER/SYS:
KEYWORDS:{keywords}

======================================================================
{applies}
{body}

Additional query words: {query}

======================================================================
{footer}

=============================================================================

THE INFORMATION PROVIDED IN THE MICROSOFT KNOWLEDGE BASE IS
PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND.  MICROSOFT DISCLAIMS
ALL WARRANTIES, EITHER EXPRESS OR IMPLIED, INCLUDING THE WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  IN NO
EVENT SHALL MICROSOFT CORPORATION OR ITS SUPPLIERS BE LIABLE FOR
ANY DAMAGES WHATSOEVER INCLUDING DIRECT, INDIRECT, INCIDENTAL,
CONSEQUENTIAL, LOSS OF BUSINESS PROFITS OR SPECIAL DAMAGES, EVEN IF
MICROSOFT CORPORATION OR ITS SUPPLIERS HAVE BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.  SOME STATES DO NOT ALLOW THE EXCLUSION
OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES
SO THE FOREGOING LIMITATION MAY NOT APPLY.

Copyright Microsoft Corporation 1998.
'''

    footer_template = '''\
Keywords          : {keywords}
Technology        : {kbarea}
Version           :'''

    applies_template='''\
-------------------------------------------------------------------------------
The information in this article applies to:
{items}
-------------------------------------------------------------------------------
'''

    def convert_topic(self, topic: Topic) -> Output:
        soup = BeautifulSoup(topic.html, 'lxml')
        # Q12345 -> 012, Q789012 -> 789
        kbid = self.get_kbid(soup)
        path = Path(f'{int(kbid[1:]):06}'[0:3], f'{kbid}.TXT')

        when = self.get_when(soup)
        dirname = self.get_dirname(topic)
        title = self.get_title(soup)
        product = self.get_product(soup)
        keywords = self.get_keywords(soup)
        applies = self.get_applies(soup)
        body = self.get_body(soup)
        query = self.get_query(soup)
        kbarea = self.get_kbarea(soup)
        footer = self.get_footer(soup, keywords, kbarea)

        article = self.template.format(kbid=kbid,
                                       when=when.strftime("%d-%b-%Y").upper(),
                                       dirname=dirname,
                                       title=title,
                                       product=product,
                                       keywords=keywords,
                                       applies=applies,
                                       body=body,
                                       query=query,
                                       footer=footer)

        return Output(path, article)

    def get_kbid(self, root: Tag) -> str:
        '''Get the KBID for this article (e.g. Q12345).'''
        return root.find('meta', {'name': 'KBID'}).get('content').strip()

    def get_when(self, root: Tag) -> date:
        '''Get when this article was written.'''
        when: str = root.find('meta', {'name': 'KBModify'}).get('content')
        # 1994/11/23 -> 1994-11-23
        return date.fromisoformat(when.replace('/', '-'))

    def get_dirname(self, topic: Topic) -> str:
        '''Get the directory name of this article.'''
        # Source/win3x/q24636.htm -> win3x
        return Path(topic.local).parts[-2]

    def get_title(self, root: Tag) -> str:
        '''Get the title of this article.'''
        return root.title.text

    def get_product(self, root: Tag) -> str:
        '''Get the product this article is for.'''
        return root.find('meta', {'name': 'Product'}).get('content')

    def get_keywords(self, root: Tag) -> str:
        '''Get the keywords for this article.'''
        return root.find('meta', {'name': 'Keywords'}).get('content')

    def get_applies(self, root: Tag) -> str:
        '''Get the re-formatted applicable products for this article.'''
        if not root.ul:
            return ''

        applies = ''

        for list_item in root.ul.children:
            applies += f'\n - {list_item.text.strip()}'

        return self.applies_template.format(items=applies)

    def get_body(self, root: Tag) -> str:
        '''Get the re-formatted body for this article.'''
        body = root.find_all('font')[3]

        text = ''

        # Ignore "applies"
        if body.ul:
            contents = body.contents[5:]
        else:
            contents = body.contents

        for child in contents:
            if isinstance(child, str):
                text += child
                continue

            child: Tag

            text += child.text

            if child.name == 'h2':
                text += f'\n{"=" * len(child.text)}'
            elif child.name == 'h3':
                text += f'\n{"-" * len(child.text)}'

        return text.strip()

    def get_query(self, root: Tag) -> str:
        '''Get the query words for this article.'''
        if not root.span.p or not root.span.p.text.strip():
            return ''

        # Ignore "Additional reference words:" and anything extra in the text
        lines: List[str] = root.span.p.text.strip().splitlines()
        for line in lines:
            line = line.strip()
            if line.startswith('Additional reference words: '):
                return line.partition(': ')[2]
        return ''

    def get_kbarea(self, root: Tag) -> str:
        '''Get the KBArea for this article.'''
        return root.find('meta', {'name': 'KBArea'}).get('content')

    def get_footer(self, root: Tag, keywords: str, kbarea: str) -> str:
        '''Get the footer for this article.'''
        span = root.span

        # Article has pre-formatted footer
        # if span.p is None:
        #     return span.text

        return self.footer_template.format(keywords=keywords,
                                           kbarea=kbarea).strip()
