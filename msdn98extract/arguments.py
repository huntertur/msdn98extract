from dataclasses import dataclass
from os import PathLike

@dataclass
class Arguments:
    '''Program command line arguments.'''
    chm: PathLike
    outdir: PathLike
    output_type: str
    input_type: str
