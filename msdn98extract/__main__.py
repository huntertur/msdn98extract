from argparse import ArgumentParser
import logging
from multiprocessing import Pool
import os
import time
from typing import Dict

from .arguments import Arguments
from .converter import Converter, KbarchiveConverter, NullConverter
from .extractor import Extractor
from .saver import save

CONVERTERS: Dict[str, Converter] = {
    'chm': NullConverter(),
    'kbarchive': KbarchiveConverter(),
};

LOGGER = logging.getLogger(__name__)

def parse_args() -> Arguments:
    '''Parse arguments from the command line.'''
    parser = ArgumentParser(description='Extract KB articles from MSDN 98.')
    parser.add_argument('chm', help='path to KB.CHM')
    parser.add_argument('outdir', help='output directory')
    parser.add_argument('-t', '--output-type', choices=CONVERTERS.keys(),
                        default='chm', help='output directory and file format')
    parser.add_argument('-i', '--input-type', choices=('1998', '1999'),
                        default='1998', help='input help file type')

    namespace = parser.parse_args()
    args = Arguments(namespace.chm, namespace.outdir, namespace.output_type,
                     namespace.input_type)

    return args

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    args = parse_args()
    topics = Extractor(args.chm, args.outdir, args.input_type).extract_topics()

    converter = CONVERTERS[args.output_type]

    LOGGER.info('Converting documents...')
    start = time.time()

    outputs = []

    with Pool() as pool:
        for batch in pool.imap(converter.convert_topic, topics,
                               chunksize=len(topics) // os.cpu_count() + 1):
            outputs.append(batch)

    end = time.time()
    LOGGER.info('Documents converted in %f seconds', end - start)

    save(args.outdir, outputs)
