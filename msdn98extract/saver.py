import logging
from os import PathLike
from pathlib import Path
import time
from typing import List

from .converter import Output

LOGGER = logging.getLogger(__name__)

def save(outdir: PathLike, documents: List[Output]):
    '''Save all documents to an output directory.'''
    out_path = Path(outdir)

    LOGGER.info('Saving articles...')
    start = time.time()

    for document in documents:
        path = out_path / document.path
        path.parent.mkdir(exist_ok=True)
        path.write_text(document.content)

    end = time.time()
    LOGGER.info('Saved articles in %f seconds', end - start)
