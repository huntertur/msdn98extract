from dataclasses import dataclass
from typing import Set

@dataclass
class Topic:
    '''A topic describing a KnowledgeBase article.'''
    name: str
    local: str
    html: str

@dataclass
class ParentTopic:
    '''A topic describing a KnowledgeBase category.'''
    name: str
    articles: Set[Topic]
