# msdn98extract

Utility for extracting Microsoft KnowledgeBase articles from the MSDN 98 CD.
Now also supports the MSDN 1999 CD.

This utility was created to assist in adding articles to Jeff Parsons's
[Microsoft KnowledgeBase Archive](https://github.com/jeffpar/kbarchive).
Unfortunately, attempts to contact the archive curator failed. The code has
since been adapted for the bulk upload functionality of
[helparchive](https://gitlab.com/huntertur/helparchive).

Tested using Python 3.8.6 on a derivative of Debian Linux and the American
English (`1033`) edition of the MSDN 98 CD.

## License

Due to this program using [PyCHM](https://github.com/dottedmag/pychm), which
is licensed under the GNU General Public License, version 2 or later,
this program must also be (and is) licensed under the same terms. See `COPYING`
and `LICENSE`.

## Usage

1. Using a second computer:
    1. Insert the MSDN 98 CD
    2. Install the full contents of the CD
    3. Navigate to the installation directory
        * Example: `C:\Program Files\Microsoft Visual Studio\MSDN98\98VS\1033`
    4. Copy `KB.CHM` to the main computer
2. Using the main computer:
    1. `python3 -m msdn98extract KB.CHM out`
    2. (Optional) Append `-t kbarchive` for kbarchive-style directory and files

If you are using the 1999 CD, append `-i 1999`.

Please have 4 GiB of free RAM (for eight processors) and 384 MiB of disk space
available.

## Local Setup

```sh
python3 -m venv env
. env/bin/activate
apt install libchm-dev # or equivalent
pip3 install -r requirements.txt
```
